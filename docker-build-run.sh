#Actual Docker build & run script
docker build -t app .
docker rm -f app-container
docker run -d --name app-container --net=host --env-file=.env app:latest