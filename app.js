var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mysql'
});

//Sorry, a bit dirty. Better solution would be to use sequelize migration tools.
if (process.env.NODE_ENV !== 'test') {
  sequelize.sync({ alter: true });
  console.log('Models synchorized');
}

var adminRouter = require('./routes/admin')(sequelize);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Since they are all API at the moment and share a common prefix
//we can prefix the route to minimize having to repeat the prefix
app.use('/api', adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  //res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};
  //Log all errors to console
  console.error({
    message: err.message,
    error: err,
    statusCode: err.status || 500
  })

  // render the error JSON
  res.status(err.status || 500);
  if (req.app.get('env') === 'development' || err.expose) {
    res.send({ message: err.message });
  } else {
    res.send({ message: 'A server error has occurred. We apologize for the inconvenience.' });
  }
});

module.exports = app;
