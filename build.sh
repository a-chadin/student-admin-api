#Script for setting up environment to run Docker container in

sudo apt-get update -y
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo groupadd docker
sudo usermod -aG docker ssm-user

sudo apt-get install mysql-server -y

cd /home/ssm-user
#SSH is not set up, needs to enter credentials
git clone https://a-chadin@bitbucket.org/a-chadin/student-admin-api.git app
cd app

#Actual Docker build & run script
docker build -t app .
docker rm -f app-container
docker run -d --name app-container --net=host --env-file=.env app:latest