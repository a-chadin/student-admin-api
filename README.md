# Student Admin API

This app allows teachers to perform administrative functions for their students.

Location: http://54.179.233.91

APIs: 
* POST http://54.179.233.91/api/register
* GET http://54.179.233.91/api/commonstudents
* POST http://54.179.233.91/api/suspend
* POST http://54.179.233.91/api/retrievefornotifications

Sample API call:
```
curl --location --request GET 'http://localhost:3000/api/commonstudents?teacher=teacherken@gmail.com'

curl --location --request POST 'http://54.179.233.91/api/retrievefornotifications' \
--header 'Content-Type: application/json' \
--data-raw '{
  "teacher": "teacherken@gmail.com",
  "notification": "foo"
}'

curl --location --request POST 'http://54.179.233.91/api/retrievefornotifications' \
--header 'Content-Type: application/json' \
--data-raw '{
  "teacher": "teacherjoe@gmail.com",
  "notification": "foo"
}'
```

## System dependencies

* Docker
* git
* MySQL database

## Deployment instructions

This app has been containerized.

### Deploy on local

See `sample.env` file for required environment variables.

#### Deploy on Docker

See `build.sh` for scripts on setting up Docker on an Ubuntu machine.

With Docker and Git installed: 

```
git clone https://a-chadin@bitbucket.org/a-chadin/student-admin-api.git
docker build -t app .
docker run -d --name app-container --net=host --env-file=.env app
```

#### Run without Docker

With Node.js 12 installed:

```
npm install
npm start
```

## How to test

```
node_modules/mocha/bin/mocha --require test/hooks.js --grep "Model tests" --exit
node_modules/mocha/bin/mocha --require test/hooks.js --grep "API" --exit
```

API and model tests unfortunately are not currently tuned to run together, as they rely on the same starting database fixtures loaded by `hooks.js`.