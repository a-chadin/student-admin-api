require('dotenv').config({ path: '.testenv' });

const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mysql'
});

const { Student, Teacher } = require('../models/models')(sequelize);

exports.mochaHooks = {
    async beforeAll() {
        await sequelize.sync({ force: true });

        tKen = await Teacher.create({ name: 'teacherken@gmail.com' });
        tJoe = await Teacher.create({ name: 'teacherjoe@gmail.com' });

        sJon = await Student.create({
            name: 'studentjon@gmail.com'
        })
        await sJon.addTeacher(tKen);

        sHon = await Student.create({
            name: 'studenthon@gmail.com'
        })
        await sHon.addTeacher(tKen);

        sCommon1 = await Student.create({
            name: 'commonstudent1@gmail.com'
        })
        await sCommon1.addTeacher(tKen);
        await sCommon1.addTeacher(tJoe);

        sCommon2 = await Student.create({
            name: 'commonstudent2@gmail.com'
        })
        await sCommon2.addTeacher(tKen);
        await sCommon2.addTeacher(tJoe);

        sAgnes = await Student.create({
            name: 'studentagnes@gmail.com'
        })
        await sAgnes.addTeacher(tJoe);

        sMiche = await Student.create({
            name: 'studentmiche@gmail.com'
        })
        await sMiche.addTeacher(tJoe);
    },
    async afterAll() {
        //Leave the tables up for examination
        //await sequelize.drop();
    }
  };