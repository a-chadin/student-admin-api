FROM node:12

WORKDIR /home/node/app
COPY . /home/node/app

RUN npm install

CMD ["npm", "start"]
#By default, the container listens on port 3000
#but this can be specified using PORT env var
EXPOSE 3000