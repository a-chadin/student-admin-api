
const { DataTypes, Model, QueryTypes, Op } = require('sequelize');

module.exports = function (sequelize) {

  class Student extends Model {
    static async findStudentsWhoCanReceiveNotification(teacherName, studentNames) {
      if (studentNames.length == 0) {
        studentNames = null;
      }

      //SELECT DISTINCT s FROM s JOIN ts JOIN t WHERE s.suspended = 0 AND (t.name = ? OR s.name in ?)
      const students = await sequelize.query("SELECT DISTINCT s.* \
            FROM Students s JOIN TeacherStudents ts ON s.id = ts.studentId \
            JOIN Teachers t ON t.id = ts.TeacherId \
            WHERE s.suspended = :suspended AND (t.name = :tName OR s.name IN (:sNames))",
        {
          replacements: {
            suspended: false,
            tName: teacherName,
            sNames: studentNames
          },
          type: QueryTypes.SELECT,
          model: Student,
          mapToModel: true
        });

      return students;
    }
    static async findCommon(teacherNames) {
      //SELECT s FROM s JOIN ts JOIN t WHERE t.name = ? HAVING COUNT(*) = ?
      if (teacherNames.length == 0) {
        return [];
      }

      //SELECT DISTINCT s FROM s JOIN ts JOIN t WHERE s.suspended = 0 AND (t.name = ? OR s.name in ?)
      const idResults = await sequelize.query("SELECT s.id, COUNT(*) \
            FROM Students s JOIN TeacherStudents ts ON s.id = ts.studentId \
            JOIN Teachers t ON t.id = ts.TeacherId \
            WHERE t.name IN (:tNames) GROUP BY s.id HAVING COUNT(*) = :numTeachers",
        {
          replacements: {
            tNames: teacherNames,
            numTeachers: teacherNames.length
          },
          type: QueryTypes.SELECT
        });

      if (idResults.length == 0) {
        return [];
      }

      let studentIDs = [];
      for (const idResult of idResults) {
        studentIDs.push(idResult.id);
      }

      const students = await Student.findAll({
        where: {
          id: {
            [Op.in]: studentIDs
          }
        }
      });

      return students;
    }
  }

  Student.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    suspended: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    sequelize,
    indexes: [
      {
        unique: true,
        fields: ['name']
      }
    ]
  });

  class Teacher extends Model {
    static async registerStudents(teacherName, studentNames) {
      const [teacher, created] = await Teacher.findOrCreate({
        where: {
          name: teacherName
        }
      });

      for (const studentName of studentNames) {
        const [student, created] = await Student.findOrCreate({
          where: {
            name: studentName
          }
        });

        await teacher.addStudent(student);
      }
    }
  }

  Teacher.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    indexes: [
      {
        unique: true,
        fields: ['name']
      }
    ]
  });

  Student.belongsToMany(Teacher, { through: 'TeacherStudents' });
  Teacher.belongsToMany(Student, { through: 'TeacherStudents' });

  return { Student, Teacher };

}