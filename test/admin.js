chai = require('chai');
chaiHttp = require('chai-http');
app = require('../app');
Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mysql'
});

const { Student, Teacher } = require('../models/models')(sequelize);

chai.use(chaiHttp);
chai.should();

describe('Admin API tests', function () {
  describe('POST /api/register', function () {
    describe('Registering one or more students', function () {
      it('should be successful', async function () {
        reqBody = {
          "teacher": "teachercharlie@gmail.com",
          "students":
            [
              "studentjon@gmail.com",
              "studenthon@gmail.com"
            ]
        };
        const res = await chai.request(app)
          .post('/api/register')
          .send(reqBody);

        res.should.have.status(204);

        const teacher = await Teacher.findOne({
          where: {
            name: "teachercharlie@gmail.com"
          }
        });

        students = await teacher.getStudents();

        students.length.should.equal(2);
      });
    });

    describe('Register without teacher', function () {
      it('should be return 400 with correct error format', async function () {
        reqBody = {
          "students":
            [
              "studentjon@gmail.com",
              "studenthon@gmail.com"
            ]
        };
        const res = await chai.request(app)
          .post('/api/register')
          .send(reqBody);

        res.should.have.status(400);
        res.body.should.have.property('message');
        res.body.message.should.be.a('string');
        console.log(res.body);
      });
    });

    describe('Register without student', function () {
      it('should be return 400 with correct error format', async function () {
        reqBody = {
          "teacher": "teacherken@gmail.com",
          "students":
            [
            ]
        };
        const res = await chai.request(app)
          .post('/api/register')
          .send(reqBody);

        res.should.have.status(400);
        res.body.should.have.property('message');
        res.body.message.should.be.a('string');
        console.log(res.body);
      });
    });
  });

  describe('GET /api/commonstudents', function () {
    describe('Get using one teacher', function () {
      it('should be successful', async function () {
        const res = await chai.request(app)
          .get('/api/commonstudents')
          .query({ teacher: 'teacherken@gmail.com' });

        res.should.have.status(200);
        res.body.should.have.property('students');
        res.body.students.should.be.a('array');
        res.body.students.should.include.members(['studentjon@gmail.com'])
      });
    });

    describe('Get using two teachers', function () {
      it('should be successful', async function () {
        const res = await chai.request(app)
          .get('/api/commonstudents')
          .query({ teacher: ['teacherken@gmail.com', 'teacherjoe@gmail.com'] });

        res.should.have.status(200);
        res.body.should.have.property('students');
        res.body.students.should.be.a('array');
        res.body.students.should.include.members(['commonstudent1@gmail.com']);
        res.body.students.should.not.include.members(['studentjon@gmail.com']);
      });
    });
  });

  describe('POST /api/suspend', function () {
    describe('Suspending an existing student', function () {
      it('should be successful', async function () {
        reqBody = {
          "student": "studentmiche@gmail.com"
        };
        const res = await chai.request(app)
          .post('/api/suspend')
          .send(reqBody);

        res.should.have.status(204);
      });
    });

    describe('Suspending non existent student', function () {
      it('should return 404', async function () {
        reqBody = {
          "student": "somenonexistent@gmail.com"
        };
        const res = await chai.request(app)
          .post('/api/suspend')
          .send(reqBody);

        res.should.have.status(404);
        res.body.should.have.property('message');
        res.body.message.should.be.a('string');
        console.log(res.body);
      });
    });

  });

  describe('POST /api/retrievefornotifications', function () {
    describe('Example 1', function () {
      it('should be successful', async function () {
        reqBody = {
          "teacher": "teacherken@gmail.com",
          "notification": "Hello students! @studentagnes@gmail.com @studentmiche@gmail.com"
        };
        const res = await chai.request(app)
          .post('/api/retrievefornotifications')
          .send(reqBody);

        res.should.have.status(200);
        res.body.should.have.property('recipients');
        res.body.recipients.should.be.a('array');
      });
    });

    describe('Example 2', function () {
      it('should be successful', async function () {
        reqBody = {
          "teacher": "teacherken@gmail.com",
          "notification": "Hey everybody"
        };
        const res = await chai.request(app)
          .post('/api/retrievefornotifications')
          .send(reqBody);

        res.should.have.status(200);
        res.body.should.have.property('recipients');
        res.body.recipients.should.be.a('array');
      });
    });

    describe('Notification for non-existent teacher', function () {
      it('should be successful', async function () {
        reqBody = {
          "teacher": "nonexistent@gmail.com",
          "notification": "Hey everybody"
        };
        const res = await chai.request(app)
          .post('/api/retrievefornotifications')
          .send(reqBody);

        res.should.have.status(200);
        res.body.should.have.property('recipients');
        res.body.recipients.should.be.a('array').that.is.empty;
      });
    });

  });
});