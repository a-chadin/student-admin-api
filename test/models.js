const { assert } = require('chai');

const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: 'mysql'
});

const { Student, Teacher } = require('../models/models')(sequelize);

describe('Model tests', function() {
  describe('Get list of students registered under a teacher', function() {
    it('should return correct list', async function() {
      const students = await Student.findCommon(["teacherken@gmail.com"]);

      assert.equal(students.length, 4);
    });
  });

  describe('Get list of students registered under two teachers', function() {
    it('should return correct list', async function() {
      const students = await Student.findCommon(["teacherken@gmail.com", "teacherjoe@gmail.com"]);

      assert.equal(students.length, 2);
    });
  });

  describe('Register a student', function() {
    it('should be successful', async function() {

      const tKen = await Teacher.findOne({
        where: {
          name: "teacherken@gmail.com"
        }
      });

      students = await tKen.getStudents();

      numStudents = students.length;

      await Teacher.registerStudents(
        "teacherken@gmail.com",
        ["alice@gmail.com"]
      );

      students = await tKen.getStudents();

      assert.equal(students.length, numStudents + 1);
    });
  });

  describe('Register a student twice', function() {
    it('should be successful', async function() {

      const tKen = await Teacher.findOne({
        where: {
          name: "teacherken@gmail.com"
        }
      });

      students = await tKen.getStudents();

      numStudents = students.length;

      await Teacher.registerStudents(
        "teacherken@gmail.com",
        ["bob@gmail.com"]
      );

      students = await tKen.getStudents();

      assert.equal(students.length, numStudents + 1);

      await Teacher.registerStudents(
        "teacherken@gmail.com",
        ["bob@gmail.com"]
      );

      students = await tKen.getStudents();

      assert.equal(students.length, numStudents + 1);
    });
  });

  describe('Suspend a student', function() {
    it('should be successful', async function() {
      sAlice = await Student.findOne({
        where: {
          name: "alice@gmail.com"
        }
      });
      sAlice.suspended = true;
      await sAlice.save();

      sAlice = await Student.findOne({
        where: {
          name: "alice@gmail.com"
        }
      });

      assert.equal(sAlice.suspended, true);
    });
  });

  describe('Get list of students who can receive a given notification ken', function() {
    it('should be successful', async function() {
      tName = "teacherken@gmail.com";
      mentions = ["studentagnes@gmail.com", "studentmiche@gmail.com"];

      const students = await Student.findStudentsWhoCanReceiveNotification(tName, mentions);
      
      assert.equal(students.length, 7);
    });
  });


  describe('Get list of students who can receive a given notification with zero mention', function() {
    it('should be successful', async function() {
      tName = "teacherken@gmail.com";
      mentions = [];

      const students = await Student.findStudentsWhoCanReceiveNotification(tName, mentions);
      
      assert.equal(students.length, 5);
    });
  });

  describe('Get list of students who can receive a given notification with teacher jon', function() {
    it('should be successful', async function() {
      tName = "teacherjoe@gmail.com";
      mentions = [];

      const students = await Student.findStudentsWhoCanReceiveNotification(tName, mentions);

      assert.equal(students.length, 4);
    });
  });


  describe('Get list of students who can receive a given notification with non-existent teacher', function() {
    it('should be successful', async function() {
      tName = "teacherjon@gmail.com";
      mentions = [];

      const students = await Student.findStudentsWhoCanReceiveNotification(tName, mentions);

      assert.equal(students.length, 0);
    });
  });
});