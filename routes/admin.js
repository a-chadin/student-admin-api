var express = require('express');
var router = express.Router();
const Joi = require('joi');
var util = require('util');
const asyncHandler = require('express-async-handler');
var createError = require('http-errors');

module.exports = function (sequelize) {

  const { Student, Teacher } = require('../models/models')(sequelize);

  const registerRequestSchema = Joi.object({
    teacher: Joi.string()
      .required(),
    students: Joi.array()
      .min(1)
      .items(Joi.string())
  });
  
  function convertValidationErrorToString(valErr) {
    errMsgs = [];
    for (const detail of valErr.details) {
      errMsgs.push(detail.message);
    }
  
    errStr = util.format('Invalid request body: %s', errMsgs.join(', '));
    return errStr;
  }
  
  router.post('/register', asyncHandler(async function (req, res, next) {
    const { error, value } = registerRequestSchema.validate(req.body);
    if (error !== undefined) {
      next(createError(400, convertValidationErrorToString(error)));
      return;
    }
  
    await Teacher.registerStudents(value.teacher, value.students);
  
    res.sendStatus(204);
  }));
  
  const commonStudentsQueryParamSchema = Joi.object({
    //if a single teacher is specified, req.query.teacher will give a string
    //if multiple teachers are specified, req.query.teacher will give an array
    //need to handle both
    teachers: Joi.alternatives().try(Joi.array().min(1).items(Joi.string()), Joi.string())
      .required()
  });
  
  router.get('/commonstudents', asyncHandler(async function (req, res, next) {
    queryParams = {
      teachers: req.query.teacher
    };
    const { error, value } = commonStudentsQueryParamSchema.validate(queryParams);
    if (error !== undefined) {
      next(createError(400, convertValidationErrorToString(error)));
      return;
    }
  
    //standardize between string and array
    if (typeof value.teachers === 'string') {
      value.teachers = [value.teachers];
    }
  
    const students = await Student.findCommon(value.teachers);
  
    studentNames = [];
    for (const student of students) {
      studentNames.push(student.name);
    }
    res.send({
      "students": studentNames
    });
  }));
  
  const suspendRequestSchema = Joi.object({
    student: Joi.string()
      .required()
  });
  
  router.post('/suspend', asyncHandler(async function (req, res, next) {
    const { error, value } = suspendRequestSchema.validate(req.body);
    if (error !== undefined) {
      next(createError(400, convertValidationErrorToString(error)));
      return;
    }
  
    const student = await Student.findOne({
      where: {
        name: value.student
      }
    });
    if (student === null) {
      //Student not found
      next(createError(404, util.format('Student %s not found', value.student)));
      return;
    }
  
    student.suspended = true;
    await student.save();
  
    res.sendStatus(204);
  }));
  
  const retrieveNotificationsRequestSchema = Joi.object({
    teacher: Joi.string()
      .required(),
    notification: Joi.string()
      .required()
  });
  
  router.post('/retrievefornotifications', asyncHandler(async function (req, res, next) {
    const { error, value } = retrieveNotificationsRequestSchema.validate(req.body);
    if (error !== undefined) {
      next(createError(400, convertValidationErrorToString(error)));
      return;
    }
  
    //This will match all start of the @ and terminate at space, as per example
    regex = /@[^ ]*/g;
    mentions = value.notification.match(regex);
  
    studentNames = [];
    if (mentions !== null) {
      for (const mention of mentions) {
        //Strip the @ at the start
        studentNames.push(mention.substr(1));
      }
    }
    const students = await Student.findStudentsWhoCanReceiveNotification(value.teacher, studentNames);
  
    recipients = [];
    for (const student of students) {
      recipients.push(student.name);
    }
    res.send({
      "recipients": recipients
    });
  }));
  
  return router;
  
}